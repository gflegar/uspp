#ifndef HUBS_AND_AUTH_H_
#define HUBS_AND_AUTH_H_


#include <iostream>
#include <vector>
#include <tuple>


#include "graph_matrix.h"
#include "num_utils.h"
#include "fixed_point.h"


namespace wpr {


/**
 * Calculate authority score using the HITS algorithm.
 *
 * @param[in] g  a GraphMatrix
 * @param[in] iters  maximal number of iterations to perform
 * @param[in] eps  desierable accuracy
 * @param[in] progress  a stream used to print the progress of the algorithm in
 *                      human-readable format
 *
 * @return a tuple `(x, err, it)`
 *      - `x` -- the computed authority score vector
 *      - `err` -- error of the authority vector
 *      - `it` -- number of iterations performed
 */
template <typename T>
std::tuple<std::vector<T>, T, int> authority_score(
        const GraphMatrix &g,
        int iters, T eps = T(0),
        std::ostream& progress = std::clog)
{
    auto x = std::vector<T>(g.dim(), T(1));
    mult_ip<T>(T(1)/x.size(), x);

    auto func = [&g](std::vector<T> x) {
        std::vector<T> r = trans_mult(g, mult(g,x));
        auto lambda = dot_prod(x, r);
        mult_ip(lambda, x);
        T err = dist(r, x) / lambda;
        mult_ip(T(1)/norm(r), r);
        return make_tuple(r, err);
    };

    return fixed_point(x, iters, eps, func, progress);
}


/**
 * Calculate hub score using the HITS algorithm.
 *
 * @param[in] g  a GraphMatrix
 * @param[in] iters  maximal number of iterations to perform
 * @param[in] eps  desierable accuracy
 * @param[in] progress  a stream used to print the progress of the algorithm in
 *                      human-readable format
 *
 * @return a tuple `(x, err, it)`
 *      - `x` -- the computed hub score vector
 *      - `err` -- error of the authority vector
 *      - `it` -- number of iterations performed
 */
template<typename T>
std::tuple<std::vector<T>, T, int> hub_score(
        const GraphMatrix &g,
        int iters, T eps = T(0),
        std::ostream& progress = std::clog)
{
    auto x = std::vector<T>(g.dim(), T(1));
    mult_ip<T>(T(1)/x.size(), x);

    auto func = [&g](std::vector<T> x) {
        std::vector<T> r = mult(g, trans_mult(g,x));
        auto lambda = dot_prod(x, r);
        mult_ip(lambda, x);
        T err = dist(r, x) / lambda;
        mult_ip(T(1)/norm(r), r);
        return make_tuple(r, err);
    };

    return fixed_point(x, iters, eps, func, progress);
}


}

#endif
