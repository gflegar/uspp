#ifndef NUM_UTILS_H_
#define NUM_UTILS_H_


#include <vector>
#include <cstdlib>
#include <cmath>
#include <numeric>
#include <algorithm>


namespace wpr {


/**
 * Calculate distance from `x` to `y`.
 *
 * @param[in] x  first vector
 * @param[in] y  second vector
 *
 * @return Distance from `x` to `y`.
 */
template <typename T>
T dist(const std::vector<T> &x, const std::vector<T> &y)
{
    T ds = T(0);
    for (std::size_t i = 0; i < x.size(); ++i) {
        T tmp = x[i] - y[i];
        ds += tmp*tmp;
    }
    return sqrt(ds);
}


/**
 * Calculate dot product ov vectors `x` and `y`
 *
 * @param[in] x  a vector
 * @param[in] y  a vector
 *
 * @return Dot product of `x` and `y`.
 */
template <typename T>
T dot_prod(const std::vector<T> &x, const std::vector<T> &y)
{
    typedef typename std::vector<T>::size_type size_type;
    T r = T(0);
    for (size_type i = 0; i < x.size(); ++i) {
        r += x[i] * y[i];
    }
    return r;
}


/**
 * Calculate 2-norm of vector `x`.
 *
 * @param[in] x  a vector
 *
 * @return 2-norm of `x`.
 */
template <typename T>
inline T norm(const std::vector<T> &x)
{
    return sqrt(dot_prod(x, x));
}


/**
 * Calculate 1-norm of vector `x`.
 *
 * @param[in] x  a vector
 *
 * @return 1-norm of `x`.
 */
template <typename T>
T one_norm(const std::vector<T> &x)
{
    T nrm = T(0);
    for (auto v : x) {
        nrm += (v > T(0)) ? v : -v;
    }
    return nrm;
}


/**
 * Multiply a vector by scalar in place.
 *
 * @param[in] alpha  a scalar to multiply with
 * @param[in,out] x  a vector that should be multiplied
 */
template <typename T>
void mult_ip(T alpha, std::vector<T> &x)
{
    for (T &c : x) {
        c *= alpha;
    }
}


/**
 * Order the indeces of `score` by values in score in descending order.
 *
 * @param[in] score  the score vector
 *
 * @return A vector `v` such that `score[v[i]] >= score[v[j]]` for `i < j`.
 */
template <typename T>
std::vector<typename std::vector<T>::size_type> order_by_score(
        const std::vector<T> &score)
{
    typedef typename std::vector<T>::size_type size_type;
    std::vector<size_type> ord(score.size());
    std::iota(ord.begin(), ord.end(), size_type(0));
    std::sort(ord.begin(), ord.end(), [&score](size_type i, size_type j) {
            return score[i] > score[j];
    });
    return ord;
}


}
#endif //NUM_UTILS_H_

