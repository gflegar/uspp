#ifndef GRAPH_MATRIX_H_
#define GRAPH_MATRIX_H_


#include <vector>
#include <ostream>
#include <algorithm>


//#define _SAVE_MEM


/**
 * The wpr namespace contains classes and function for storing and performing
 * operations on adjacency matrices of directed graphs.
 */
namespace wpr {


class GraphMatrix;


std::ostream& operator<<(std::ostream&, const GraphMatrix&);

template <typename T>
std::vector<T> mult(const GraphMatrix&, const std::vector<T>&);

template <typename T>
std::vector<T> trans_mult(const GraphMatrix&, const std::vector<T>&);

/**
 * The GraphMatrix class stores the adjacency matrix of a directed graph.
 *
 * The adjacency matrix is stored in sparse format, a variant of CSR format
 * with only two arrays: column indeces of ones and row pointers (value array
 * is not needed since the only non-zero values are ones).
 */
class GraphMatrix {

    friend std::ostream& operator<<(std::ostream&, const GraphMatrix&);
    template <typename T>
    friend std::vector<T> mult(const GraphMatrix&, const std::vector<T>&);
    template <typename T>
    friend std::vector<T> trans_mult(const GraphMatrix&,
                                     const std::vector<T>&);
    template <typename T>
    friend std::vector<T> prank_mult(const GraphMatrix&,
                                     const std::vector<T>&);

public:
    /**
     * Type of matrix indeces.
     */
    typedef std::size_t size_type;

    /**
     * Create an empty (0 x 0) GraphMatrix.
     */
    GraphMatrix() {}

    /**
     * Create a GraphMatrix from an input stream.
     *
     * See GraphMatrix::read_from for details.
     *
     * @param[in,out] is  input stream from which the graph is read
     */
    template<typename InputStream>
    GraphMatrix(InputStream &is) { read_from(is); }

    /**
     * Read a GraphMatrix from an input stream.
     *
     * Each line of stream should contain two integers `a` and `b` representing
     * an edge from node `a` to node `b`.
     *
     * @param[in,out] is  input stream from which the graph is read
     */
    template<typename InputStream>
    void read_from(InputStream &is);

    /**
     * Get GraphMatrix's dimension.
     *
     * @return Dimension of this GraphMatrix.
     */
    size_type dim() const { return row_start.size() - 1; }

    /**
     * Get the number of ones in this matrix.
     *
     * @return Number of ones in the matrix.
     */
    size_type links() const { return col_idx.size(); }

    /**
     * Get nmber of rows with no non-zero elements.
     *
     * @return Number of rows with no non-zero elements.
     */
    size_type zero_rows() const
    {
        size_type res = 0;
        for (size_type i = 0; i < row_start.size() - 1; ++i) {
            if (row_start[i] == row_start[i+1]) {
                ++res;
            }
        }
        return res;
    }

private:

    /**
     * Column indeces.
     */
    std::vector<size_type> col_idx;

    /**
     * Row pointers.
     */
    std::vector<size_type> row_start;
};


template <typename InputStream>
void GraphMatrix::read_from(InputStream &is)
{
#ifdef _SAVE_MEM
    size_type max_idx = 0;
    std::vector<std::pair<size_type, size_type>> links;
    std::pair<size_type, size_type> tmp;
    while (is >> tmp.first >> tmp.second) {
        links.push_back(tmp);
        max_idx = std::max(max_idx, std::max(tmp.first, tmp.second));
    }
    std::sort(links.begin(), links.end());

    col_idx.reserve(links.size());
    row_start.reserve(max_idx+1);
    size_type last_row = 0;
    for (auto &&lk : links) {
        while (last_row <= lk.first) {
            ++last_row;
            row_start.push_back(col_idx.size());
        }
        col_idx.push_back(lk.second);
    }
    row_start.push_back(col_idx.size());
#else
    std::vector<std::vector<size_type>> matrix;
    size_type a, b;
    while (is >> a >> b) {
        if (matrix.size() <= a) {
            matrix.resize(a+1);
        }
        matrix[a].push_back(b);
    }
    row_start.push_back(0);
    for (auto &row : matrix) {
        std::sort(row.begin(), row.end());
        col_idx.insert(col_idx.end(), row.begin(), row.end());
        row_start.push_back(row_start.back() + row.size());
    }
#endif
}


/**
 * Calculate \f$ y = gx \f$ and return \f$y\f$.
 *
 * @param[in] g  a GraphMatrix
 * @param[in] x  a vector
 *
 * @return \f$y\f$ such that \f$ y = gx \f$.
 */
template <typename T>
std::vector<T> mult(const GraphMatrix &g, const std::vector<T> &x)
{
    typedef GraphMatrix::size_type stype;
    std::vector<T> result(x.size(), T(0));
    for (stype i = 0; i < g.dim(); ++i) {
        for (stype j = g.row_start[i]; j < g.row_start[i+1]; ++j) {
            result[i] += x[g.col_idx[j]];
        }
    }
    return result;
}


/**
 * Calculate \f$ y = g^Tx \f$ and return \f$y\f$.
 *
 * @param[in] g a GraphMatrix
 * @param[in] x a vector
 *
 * @return \f$y\f$ such that \f$ y = g^Tx \f$
 */
template <typename T>
std::vector<T> trans_mult(const GraphMatrix &g, const std::vector<T> &x)
{
    typedef GraphMatrix::size_type stype;
    std::vector<T> result(x.size(), T(0));
    for (stype i = 0; i < g.dim(); ++i) {
        for (stype j = g.row_start[i]; j < g.row_start[i+1]; ++j) {
            result[g.col_idx[j]] += x[i];
        }
    }
    return result;
}


/**
 * Calculate \f$ y = PR(g)x \f$ and return \f$y\f$.
 *
 * \f$ PR(g) \f$ is a matrix obtained from `g` by transposing it, dividing each
 * non-zero column with the number of non-zero elemnts in that column and
 * filling zero columns with `1/dim(g)`.
 *
 * @param[in] g  a GraphMatrix
 * @param[in] x  a vector
 *
 * @return \f$y\f$ such that \f$ y = PR(g)x \f$
 */
template <typename T>
std::vector<T> prank_mult(const GraphMatrix &g, const std::vector<T> &x)
{
    typedef GraphMatrix::size_type stype;
    std::vector<T> result(x.size(), T(0));
    T dangling_sum = T(0);
    for (stype i = 0; i < g.dim(); ++i) {
        stype rsize = g.row_start[i+1] - g.row_start[i];
        for (stype j = g.row_start[i]; j < g.row_start[i+1]; ++j) {
            result[g.col_idx[j]] += x[i] / rsize;
        }
        if (rsize == 0) {
            dangling_sum += x[i];
        }
    }
    for (auto &v : result) {
        v += dangling_sum / g.dim();
    }
    return result;
}


/**
 * Print a given GraphMatrix to the output stream in human-readable form.
 *
 * @param[in,out] os  an output stream to which the matrix is printed
 * @param[in] G  a GraphMatrix
 *
 * @return `os`
 */
std::ostream& operator<<(std::ostream &os, const GraphMatrix &G)
{
    using stype = GraphMatrix::size_type;
    for (stype i = 0; i < G.dim(); ++i) {
        os << i << ": ";
        for (stype j = G.row_start[i]; j < G.row_start[i+1]; ++j) {
            os << G.col_idx[j] << ' ';
        }
        os << '\n';
    }
    return os;
}


}
#endif //GRAPH_MATRIX_H_

