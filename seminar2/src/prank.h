#ifndef PRANK_H_
#define PRANK_H_


#include <iostream>
#include <vector>
#include <tuple>


#include "graph_matrix.h"
#include "num_utils.h"
#include "fixed_point.h"


namespace wpr {


/**
 * Calculate PageRank score of `g`.
 *
 * @param[in] g  a GraphMatrix
 * @param[in] m  random surfer jump ratio
 * @param[in] iters  maximal number of iterations to perform
 * @param[in] eps  desiarable accuracy
 * @param[in] start  starting score approximation
 * @param[in] progress  a stream used to print the progres of the algorithm in
 *                      human-readable format
 *
 * @return a tuple `(x, err, it)`
 *      - `x` -- PageRank score vector
 *      - `err` -- error of the score vector
 *      - `it` -- number of iterations performed
 */
template <typename T>
std::tuple<std::vector<T>, T, int> prank_score(
        const GraphMatrix &g,
        T m, int iters, T eps = T(0),
        std::vector<T> start = std::vector<T>(),
        std::ostream& progress = std::clog)
{
    if (start.size() == 0) {
        start.resize(g.dim(), T(1));
    }
    mult_ip(T(1)/one_norm(start), start);

    auto func = [&g, m](std::vector<T> x) {
        auto r = prank_mult(g, x);
        for (auto &v : r) {
            v = (T(1) - m) * v + m / T(r.size());
        }
        mult_ip(T(1)/one_norm(r), r);
        return make_tuple(r, dist(x, r));
    };

    return fixed_point(start, iters, eps, func, progress);
}


}


#endif

