#ifndef FIXED_POINT_H_
#define FIXED_POINT_H_


#include <vector>
#include <iostream>
#include <tuple>


namespace wpr {


/**
 * Calculate a fixed point of the function `func`.
 *
 * This function uses fixed-point iteration \f$x_{n+1} = first(func(x_n))\f$ to
 * calculate the fixed point. Iterations stop when the condition
 * \f$ n > iters \vee second(func(x_n)) < eps \f$ is first fulfilled (so it can
 * be used for problems slightly diferent than fixed point).
 *
 * @tparam T  a numeric type
 * @tparam Func  a functor with operator
 *               `std::tuple<std::vector<T>,T> operator ()(
 *               const std::vector<T>&)`.
 *
 * @param[in] start  starting point \f$ x_0 \f$
 * @param[in] func  a function
 * @param[in] iters  maximal number of iterations to perform
 * @param[in] eps  accuracy
 * @param[in,out] progress  a stream used to print the progress of the
 *                          algorithm in human-readable format
 *
 * @return a tuple `(x, it, dx)`
 *      - `x` -- the computed fixed point
 *      - `dx` -- error of `x`
 *      - `it` -- number of iterations performed
 */
template <typename T, typename Func>
std::tuple<std::vector<T>, T, int> fixed_point(
        const std::vector<T> &start,
        int iters, T eps = T(0),
        Func func = Func(),
        std::ostream& progress = std::clog)
{
    std::vector<T> x[2];
    x[1] = start;
    T dx;
    int i;
    for (i = 0; i <= iters; ++i) {
        std::tie(x[i%2], dx) = func(x[(i+1)%2]);
        if (dx < eps) {
            break;
        }
        if (i > 0) {
            progress << i << '\t' << dx << '\n';
        }
    }
    return make_tuple(x[(i+1)%2], dx, i-1);
}


}


#endif
