#include <iostream>
#include <fstream>
#include <iterator>
#include <tuple>
#include <vector>
#include <cstdlib>
#include <string>
#include <map>

#include "hubs_and_auth.h"
#include "prank.h"


/**
 * Print usage messsage for `method` to stream.
 *
 * @param[in] os  a stream
 * @param[in] prog_name  name of the program (as in `argv[0]`)
 * @param[in] method  requested method (supported are: `""`, `"hubs"`,
 *                    `"auth"`, `"prank"`)
 */
void print_usage(std::ostream &os, const char *prog_name, const char *method)
{
    const std::map<std::string, std::string> msg {
        {"", "METHOD INFILE OUTFILE ITERS [OPTIONS]..."},
        {"hubs", "hubs INFILE OUTFILE ITERS [EPS]"},
        {"auth", "auth INFILE OUTFILE ITERS [EPS]"},
        {"prank", "prank INFILE OUTFILE ITERS [M] [EPS]"}
    };

    os << "Usage: " << prog_name << " " << msg.at(method) << std::endl;
}

/**
 * Save score vector to a file sorted by scores in descending order.
 *
 * @param scores  a score vector
 * @param filename  name of the file
 * @param precision  number of decimal places in the output
 */
template<typename T>
void save_scores(const std::vector<T> &scores, const char *filename,
                 int precision = 16)
{
    typedef typename std::vector<T>::size_type stype;
    auto ordering = wpr::order_by_score(scores);
    std::vector<stype> order(ordering.size());

    for (stype i = ordering.size(); i > 0; --i) {
        order[ordering[i-1]] = i;
    }

    std::ofstream outfile(filename);
    outfile.flags(std::ios::scientific);
    outfile.precision(precision);
    for (stype i = 0; i < scores.size(); ++i) {
        outfile << order[i] << '\t' << scores[i] << '\n';
    }
    outfile.close();
}


int main(int argc, char *argv[])
{
    if (argc < 5) {
        print_usage(std::cerr, argv[0], "");
        return 1;
    }

    const char *method = argv[1];
    const char *in_file = argv[2];
    const char *out_file = argv[3];

    int iters = atoi(argv[4]);
    double err;

    std::clog.flags(std::ios::scientific);
    std::clog.precision(16);

    std::ifstream istr(in_file);
    wpr::GraphMatrix G(istr);
    istr.close();

    std::cout << "Nodes     : " << G.dim() << std::endl
              << "Links     : " << G.links() << std::endl
              << "Empty rows: " << G.zero_rows() << std::endl;

    std::vector<double> score;

    if (std::string(method) == "hubs") {
        if (argc < 5 || argc > 6) {
            print_usage(std::cerr, argv[0], "hubs");
            return 1;
        }
        double eps = (argc == 6) ? atof(argv[6]) : 0.0;
        std::tie(score, err, iters) = wpr::hub_score(G, iters, eps);

    } else if (std::string(method) == "auth") {
        if (argc < 5 || argc > 6) {
            print_usage(std::cerr, argv[0], "auth");
            return 1;
        }
        double eps = (argc == 6) ? atof(argv[6]) : 0.0;
        std::tie(score, err, iters) = wpr::authority_score(G, iters, eps);

    } else if (std::string(method) == "prank") {
        if (argc < 5 || argc > 7) {
            print_usage(std::cerr, argv[0], "prank");
            return 1;
        }
        double m = (argc >= 6) ? atof(argv[6]) : 0.15;
        double eps = (argc == 7) ? atof(argv[7]) : 0.0;
        std::tie(score, err, iters) = wpr::prank_score(G, m, iters, eps);

    } else {
        print_usage(std::cerr, argv[0], "");
        return 1;
    }

    save_scores(score, out_file);

    std::cout << "Accuracy: " << err << std::endl
              << "Iters   : " << iters << std::endl;

    return 0;
}

