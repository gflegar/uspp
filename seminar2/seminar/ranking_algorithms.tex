\documentclass[11pt]{article}
\usepackage[utf8x]{inputenc}
\usepackage{fancyhdr}
\fancyhf{}
\renewcommand{\headrulewidth}{1pt}
\fancypagestyle{plain}{}
\pagestyle{fancy}
\usepackage[T1]{fontenc}
\usepackage[croatian]{babel}
\usepackage{amsmath,amsfonts,amssymb}
\usepackage{indentfirst}
\usepackage{amsthm}
\usepackage{dsfont}
\usepackage{hyperref}
\usepackage{algorithm2e}
\usepackage{comment}
\usepackage{graphicx}

\newtheorem{df}{Definicija}
\newtheorem{nap}[df]{Napomena}
\newtheorem{tm}[df]{Teorem}
\newtheorem{prop}[df]{Propozicija}
\newtheorem{kor}[df]{Korolar}
\newtheorem{lem}[df]{Lema}
\newtheorem{prim}[df]{Primjer}

\DeclareMathOperator{\diag}{diag}
\DeclareMathOperator{\sign}{sign}

\begin{document}

\title{Algoritmi za rangiranje \\
    Usporedba algoritama HITS i PageRank}

\author{Lana Čaldarević \\ Goran Flegar}
\date{\today}
\maketitle
\newpage

\begin{abstract}

World Wide Web je veliki repozitorij informacijskih resursa koji uključuju tekst, audio, video ... Kako je tih informacija iz dana u dan sve više, sve je teže naći traženu informaciju. Stoga korisnici danas uglavnom ovise o raznim tražilicama za pronalaženje prikladnih odgovora na njihove upite. Iz tog razloga tražilice koriste različite algoritme za rangiranje za prikaz rezultirajućih stranica u rangiranom poretku. U ovom radu predstavljamo i uspoređujemo dva algoritma za rangiranje: HITS algoritam autora Jona Kleinberga, te PageRank algoritam koji koristi Google.

\end{abstract}

\newpage
\tableofcontents
\newpage
\fancyhead[R]{\thepage}

\section{Uvod}

World Wide Web trenutačno sadrži više od 3 milijarde web stranica koje sadrže tekst, slike i ostale multimedijske sadržaje te iz tog razloga pronalazak web stranica koje su relevantne za korisnikov upit je danas važan i zahtjevan zadatak. Razvijeni su mnogi komercijalni pretraživači koji koriste ljudi diljem svijeta, međutim web stranice koje vrati pretraživač još uvijek nisu dovoljno relevantne te su potrebna daljna istraživanja i razvijanje kako bi pretraživači bili efektivniji. \\

Rangiranje web stranica je tehnika optimizacije korištena
od strane tražilica za rangiranje stotine i tisuće web stranica. Algoritmi koriste različite kriterije za rangiranje web stranica, npr. neki algoritmi koriste strukturu linkova, dok drugi gledaju na sadržaj stranice. Tako algoritme za rangiranje možemo podjeliti u dvije skupine. Mi ćemo u ovom radu opisati algoritme koji se baziraju na strukturi linkova. Ti algoritmi promatraju Web kao usmjereni graf gdje su web stranice čvorovi, a hiperlinkovi između čvorova usmjereni bridovi. \\

U ovom radu opisujemo dva najpoznatija algoritma za rangiranje, PageRank i HITS. Opisujemo matricu Weba prikazanu u sparse formatu i detalje implementacije. Prikazujemo test podatke, uspoređujemo brzine konvergencije algoritama te njihove rezultate. Na kraju opisujemo zaključke ovog rada.

\newpage

\section{HITS algoritam}

HITS algoritam primjenjuje se na skup web stranica koje su generirane pomoću nekog pretraživača nakon upita od strane korisnika. Taj skup web stranica formira usmjereni graf $G = (V,E),$ gdje je $V$ skup čvorova (tj. web stranica), a $E$ skup bridova(tj. hiperlinkova između web stranica).\\

HITS algoritam temelji se na ideji da u grafu Weba promatramo dva važna tipa čvorova: autoriteti i hub-ovi.
Na važnu i informativnu web stranicu obično pokazuje velik broj hiperlinkova i takva web stranica se zove autoritet. Web stranica koja pokazuje na mnogo autoriteta je također korisna te ju nazivamo hub. Slijedi da je dobar hub onaj koji pokazuje na mnogo dobrih autoriteta, a dobar autoritet onaj na kojeg pokazuje mnogo dobrih hub-ova. \\

Svakom čvoru $i$ dodjeljuju se dvije nenegativne vrijednosti: vrijednost autoriteta $x_i$ i vrijednost hub-a $y_i$. Na početku je svakom $x_i$ i $y_i$ dodjeljena proizvoljna nenegativna vrijednost. Dalje "ažuriramo" vrijednosti autoriteta i hub-ova na sljedeći način: 

\begin{equation}
\label{hits-eq}
x_i^{(k)} = \sum_{j:(j,i) \in E} y_j^{(k-1)}\  \  \text{i}\ \  y_i^{(k)} = \sum_{j:(i,j) \in E} x_j^{(k)} \ ,\ k = 1,2,3...
\end{equation}

Nakon toga se vrijednosti normaliziraju tako da je $\sum_j (x_j^{(k)})^2 = 1$ i $\sum_j (y_j^{(k)})^2 = 1.$\\

Gornje iteracije se pojavljuju sekvencijalno i može se pokazati da nizovi vektora $\{x_k\}$ i $\{y_k\}$ konvergiraju kada $k \to \infty$. U praksi, postupak se provodi sve dok ima značajnih promjena između uzastopnih iteracija. \\

Ovaj iterativni proces prikazuje ovisnost između autoriteta i hub-ova: ako čvor $i$ pokazuje na mnogo čvorova sa velikom vrijednosti autoriteta (tj. velikom $x$ vrijednosti), on će imati veliku vrijednost hub-a (tj. veliku $y$ vrijednost), također ako na čvor $i$ prikazuje mnogo čvorova s velikom $y$ vrijednosti onda će on imati veliku $x$ vrijednost. \\

U terminima matrica, \ref{hits-eq} možemo zapisati kao: 
\begin{equation}
\label{hits-eq-matrix}
x^{(k)} = A^Ty^{(k-1)}, \ \  y^{(k)} = Ax^{(k)},
\end{equation}
gdje je $A$ matrica susjedstva i definirana je kao $A_{ij} = 1$, ako je $(i, 
j) \in E$, odnosno ako postoji hiperlink između čvora $i$ i $j,$ a inače $0$.

Tada iterativni proces možemo zapisati kao: 
\begin{equation}
\label{iterative-process}
x^{(k)} = c_kA^TAx^{(k-1)}, \ \ y^{(k)} = c_kAA^Ty^{(k-1)},
\end{equation}
gdje su $c_k$ takvi da je $\|x^{(k)}\| = 1$ i $\|y^{(k)}\| = 1.$ \\

HITS algoritam zapravo koristi metodu potencija za računanje svojstvenog vektora koji pripada najvećoj svojstvenoj vrijednosti matrica $A^TA$ i $AA^T,$ te iz njih čitamo ocjene autoriteta i hub-ova. \\

Konvergencija algoritma osigurana je sljedećim teoremom (\cite{kleinberg}):
\begin{tm} Nizovi $x_1, x_2, x_3, ...$ i $y_1, y_2, y_3, ...$ konvergiraju k $x^*$, $y^*$ respektivno, kada $k \to \infty.$ $x^*$ je svojstveni vektor koji pripada najvećoj svojstvenoj vrijednosti matrice $A^TA$, a $y*$ matrice $AA^T.$ 

\end{tm}




\newpage

\section{PageRank algoritam}

Sljedeći algoritam za rangiranje web stranica koji ukratko opisujemo je 
Google-ov PageRank. Taj algoritam polazi od ideje da autor stranice 
stavljanjem poveznice na neku stranicu "glasa" za tu stranicu.

Pretpostavimo da imamo graf od $n$ stranica numeriranima od $1$ do $n$. Svakoj 
stranici želimo pridružiti važnost $x_i \geq 0$. Nekoj 
stranici $i$ mogli bismo pridružiti važnost $x_i = |L_i|$, 
pri čemu je $L_i = \{j \ \vert \ \text{postoji poveznica iz } j \text{ na } i 
\}$. Dakle, važnost stranice je broj "glasova" koje je ona skupila.

U ovakvom načinu rangiranja glasovi loše rangiranih stranica su jednako važni 
kao i glasovi dobro rangiranih stranica. Željeli bismo da glas dobre 
stranice više znači nego glas one loše, pa relaciju važnosti stranice $i$ 
možemo modificirati u $x_i = \sum_{j \in L_i} x_j$.

Ovaj način računanja važnosti uzima u obzir i važnost stranica s kojih 
dolaze poveznice, ali ima i neželjenih posljedica. Na primjer, vlasnici dobro 
rangirane stranice A mogli bi početi prodavati svoje glasove drugim stranicama 
i time umjetno povećati važnost velikog broja drugih stranica. Kako bismo to 
spriječili, svakoj stranici $j$ dajemo točno jedan glas koji vrijedi $x_j$. 
Ako ta stranica pokazuje na više drugih stranica, svakoj od njih daje jedan 
dio svojeg glasa pa svaka od njih dobiva $x_j / n_j$, gdje smo s $n_j$ 
označili broj poveznica koje izlaze iz $j$. Ovime relacija za važnost stranice 
postaje:
\begin{equation}
\label{prank-eq}
x_i = \sum_{j \in L_i} \frac{x_j}{n_j}, \quad i = 1,2, \ldots, n
\end{equation}

Ako s $A = (n_{ij})_{ij}$ označimo $n \times n$ matricu pri čemu je
\[a_{ij} = \begin{cases}
\frac{1}{n_j} & j \in L_i \\
0             & \text{inače}
\end{cases}
\]
tada relacija \ref{prank-eq} poprima oblik: $x = Ax$
pa se rangiranje svodi na računanje svojstvenog vektora $x$ matrice $A$ 
pridruženog svojstvenoj vrijednosti $1$ uz uvjet $x \geq 0$.

Odmah se nameće pitanje je li $1$ uopće svojstvena vrijednost matrice $A$, 
postoji li svojstveni vektor $x \geq 0$ i ako postoji je li on jedinstven.
Odmah vidimo da ako je $x$ svojstveni vektor onda je i $\alpha x$, $\alpha \in 
\mathbb{R} \setminus \{0\}$ svojstveni vektor. Ovo ne predstavlja problem jer 
nam je bitan samo međusobni poredak stranica, a ne i točna vrijednost 
važnosti, pa možemo kao rješenje uzeti svojstveni vektor $x$ za koji je $\| x 
\|_1 = 1$.

Nadalje, naša matrica $A$ ima posebnu strukturu. Svi elementi su nenegativni i 
uz uvjet da svaka stranica sadrži barem jednu poveznicu, suma svakog stupca je 
$1$. Takve matrice se nazivaju \emph{stohastičke po stupcima} i $1$ im je 
svojstvena vrijednost (vidi npr. \cite{pagerank}). Nadalje, ukoliko su još i 
svi elementi matrice $A$ pozitivni potprostor pridružen svojstvenoj 
vrijednosti $1$ je jednodimenzionalan te su sve komponente svojstvenih vektora 
istog predznaka, pa je rješenje našeg problema jedinstveno.

Jedini problem je u tome što naša matrica nije pozitivna. To možemo riješiti 
tako da odaberemo neki parametar $m \in [0, 1]$ i umjesto matrice $A$ gledamo 
matricu $M = (1-m)A + m e e^T$, $e = [1 \ldots 1]^T$. Lako se provjeri da ako 
je $A$ stohastička tada je i matrca $M$ stohastička i pozitivna za $m > 0$.

Konačno, u \cite{pagerank} je dokazan sljedeći teorem koji osigurava 
konvergenciju metode potencija za matricu $M$.
\begin{tm}
Neka je $M$ definirana kao gore uz $m \in \langle 0, 1]$. Tada postoji 
jedinstveni vektor $q > 0$ takav da je $\| q \|_1 = 1$ i $Mq = q$. 
Nadalje, za bilo koji $x_0 > 0$, $\| x_0 \|_1 = 1$ i $k \in \mathbb{N}$ je $\| 
q - M^k 
x_0 \|_1 \leq (1-\frac{m}{n})^k$ pa niz iteracija $x_{k+1} = M x_k$ konvergira 
prema vektoru $q$.
\end{tm}

\newpage

\section{Implementacija algoritama}
\subsection*{Spremanje matrice povezanosti}
Podaci na kojima ćemo testirati algoritme sastoje se od velikog broja stranica 
(reda veličine $10^5$) pa matricu povezanosti takvog grafa nije moguće 
spremiti u memoriju u standardnom formatu. Ako se pogleda ukupan broj linkova 
između stranica vidimo da je on reda veličine $10^6$, dakle, većina elemenata 
u matrici su 0, pa se ona može spremiti u sparse formatu. Za spremanje matrice 
koristimo modificirani CSR format.

U CSR (Compressed Sparse Row) formatu podaci o matrici spremaju se u tri 
polja: $val$, $col\_ind$, $row\_ptr$. Svi elementi matrice različiti od nule 
nalaze se u polju $val$ zapisani po retcima (najprije elementi iz prvog retka, 
pa iz drugog, itd.) u redosljedu u kojem se pojavljuju u tom retku. Na $i$-tom 
mjestu polja $col\_ind$ nalazi se indeks stupca $i$-tog elementa polja $val$ u 
originalnoj matrici. Na $i$-tom mjestu polja $row\_ptr$ zapisan je indeks u 
polju $val$ prvog elementa $i$-tog retka matrice.

\begin{prim}
Za matricu
\[
\begin{bmatrix}
3 & 0 & 0 & 1 & 0 \\
0 & 0 & 0 & 0 & 1 \\
0 & 0 & 0 & 0 & 0 \\
0 & 2 & 0 & 1 & 0 \\
\end{bmatrix}
\]
CSR format izgleda ovako:
\begin{align*}
val &= [3, 1, 1, 2, 1] \\
col\_ind &= [1, 4, 5, 2, 4] \\
row\_ptr &= [1, 3, 4, 4]
\end{align*}
\end{prim}

Očito je da je za matricu dimenzija $n \times m$ s $k$ ne-nul elemenata 
potrebno $O(n + k)$ memorije, što može biti značajna ušteda u odnosu na
$O(nm)$ za spremanje cijele matrice. 

U našem slučaju svi ne-nul elementi matrice povezanosti su jedinice, pa polje 
$val$ ne 
daje nikakvu dodatnu informaciju i dovoljno je pamtiti samo polja $col\_ind$ 
i $row\_ptr$. Za PageRank algoritam potrebna je transponirana matrica 
povezanosti kojoj je svaki stupac podijeljen s brojem elemenata u tom stupcu.
I u tom slučaju nije potrebno pamtiti $val$ jer se vrijednost elemenata u 
stupcu (odnosno retku originalne matrice) može izračunati na temelju 
vrijednosti polja $row\_ptr$. Time postižemo dodatnu uštedu memorije.

\subsection*{Množenje vektora sparse matricom}

Jedina operacia sa sparse matricama koja nam treba je množenje matrica-vektor 
i transponirana matrica-vektor. Množenje matrica-vektor možemo realizirati na 
slijedeći način:

Neka je $A = \begin{bmatrix}
a_1 \cdots a_n
\end{bmatrix}^T \in \mathbb{R}^{n\times m}$
matrica s retcima $a_1^T, \ldots a_n^T$, CSR prikazom $(val, col\_idx, 
row\_ptr)$ i $x \in \mathbb{R}^m$ neki vektor. 
Tada produkt $y = Ax$ 
možemo zapisati kao $y = [a_1^Tx \cdots a_n^Tx]^T$. Dakle, $i$-ta komponenta 
vektora $y$ je skalarni produkt $i$-tog retka matrice i vektora $x$. Taj 
skalarni produkt možemo računati ovako:
\begin{align*}
y_i &= a_i^Tx = \sum_{j=1}^m a_{ij}x_j = \sum_{a_{ij} \neq 0} a_{ij}x_j \\
&= \sum_{k = row\_ptr_i}^{row\_ptr_{i+1} - 1} val_k \cdot x_{col\_idx_k}
\end{align*}

Promotrimo sada množenje $A^Tz$ za $z \in \mathbb{R}^n$. Taj produkt možemo 
zapisati kao: $A^Tz = \sum_{i=1}^n z_i a_i$.
Svaki od vektora $z_i a_i$ ima komponente različite od 0 samo na mjestima 
\[col\_idx_{row\_ptr_i}, \ldots, col\_idx_{row\_ptr_{i+1} - 1}\] a 
vrijednosti im iznose \[z_i \cdot val_{row\_ptr_i}, \ldots, z_i \cdot 
val_{row\_ptr_{i+1} - 1}\]
pa je lako izračunati zbroj tih vektora.

\newpage

\section{Test podaci i rezultati testiranja}

Algoritme smo testirali na četiri grafa preuzeta s \cite{snap}.
U tablici~\ref{graph-data} nalaze se detalji o tim grafovima.
Promatrali smo brzine konvergencije tih algoritama te postoji li 
ovisnost između rangiranja dobivenih pomoću vrijednosti hub-a, autoriteta, 
odnosno PageRank-a.

\begin{table}[h]
\begin{center}
\begin{tabular}{| r | c c |}
    \hline
    & Broj stranica & Broj linkova \\
    \hline
    BerkStan & 685 231 & 7 600 595\\
    Google & 916 428 & 5 105 039\\
    NotreDame & 325 729 & 1 497 134\\
    Stanford & 281 904 & 2 312 497\\
    \hline
\end{tabular}
\end{center}
\caption{Podaci o grafovima}
\label{graph-data}
\end{table}

\subsection*{Usporedba brzine konvergencije}

U ovom odjeljku prikazujemo usporedbu brzina konvergencije HITS i PageRank 
algoritma. U slučaju PageRank algoritma greška u $k$-toj iteraciji izračunta 
je relacijom $e_k^{(pr)} = \| Mx_k - x_k \|_1$, a u slučaju HITS-a relacijama 
$e_k^{(h)} = \frac{\|By_k - \lambda_k^{(h)}y_k\|_2}{\|\lambda_k^{(h)}y_k\|_2}$ 
i $e_k^{(a)} = \frac{\|Cx_k - 
\lambda_k^{(a)}x_k\|_2}{\|\lambda_k^{(a)}x_k\|_2}$, $B = AA^T$, $C = A^TA$  
gdje su $\lambda_k^{(h)}$ i
$\lambda_k^{(a)}$ Rayleighevi kvocjenti $\lambda_k^{(a)} = x_k^TCx_k / 
x_k^Tx_k$ i $\lambda_k^{(h)} = y_k^TBy_k / y_k^Ty_k$. Sljedeća četiri grafa 
prikazuju promjenu greške kroz iteracije. Na tri od četiri testirana grafa 
PageRank konvergira brže od HITS algoritma, od čega se na dva grafa vidi 
značajnija konvergencija. Nadalje, za konvergenciju je u svim primjerima 
HITS-u trebalo znatno više od 10 iteracija koliko se navodi u \cite{hits} na 
temelju analize prosječnog slučaja.

\begin{figure}
    \includegraphics[width=0.95\textwidth]{BerkStan1}
    \caption{Berkeley and Stanford konvergencija}
    \includegraphics[width=0.95\textwidth]{Google1}
    \caption{Google konvergencija}
\end{figure}

\begin{figure}
\includegraphics[width=0.95\textwidth]{NotreDame1}
    \caption{Notre Dame konvergencija}
    \includegraphics[width=0.95\textwidth]{Stanford1}
    \caption{Stanford konvergencija}
\end{figure}

\newpage
\subsection*{Ovisnost rangiranja pomoću hub-ova, autoriteta i PageRank-a}
Za svaki od primjera rangirali smo stranice na svaki od 3 moguća načina 
(pomoću vrijednosti hub-a, autoriteta i PageRank-a) i pogledali da li ima 
sličnosti između te tri mogućnosti ili se radi o različitim 
kriterijima rangiranja. Na sljedećih nekoliko grafova prikazane su ovisnosti 
rangova (veća vrijednost znači bolji rang). Vidimo da se uistinu radi o tri 
različita kriterija. Na tri 
primjera stranice koje su dobro rangirane s obzirom na PageRank također su 
dobri autoriteti, ali na primjeru \emph{NotreDame} vidi se da to ne mora 
uvijek biti tako.

\begin{figure}
    \centering
    \includegraphics[width=0.47\textwidth]{BerkStanAuthHub}
    \includegraphics[width=0.47\textwidth]{GoogleAuthHub}
    \includegraphics[width=0.47\textwidth]{NotreDameAuthHub}
    \includegraphics[width=0.47\textwidth]{StanAuthHub}
    \caption{Ovisnost rangiranja prema vrijednosti hub-a i autoriteta}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=0.47\textwidth]{BerkStanPrankHub}
    \includegraphics[width=0.47\textwidth]{GooglePrankHub}
    \includegraphics[width=0.47\textwidth]{NotreDamePrankHub}
    \includegraphics[width=0.47\textwidth]{StanPrankHub}
    \caption{Ovisnost rangiranja prema vrijednosti hub-a i PageRank-a}
    \centering
    \includegraphics[width=0.47\textwidth]{BerkStanAuthPrank}
    \includegraphics[width=0.47\textwidth]{GoogleAuthPrank}
    \includegraphics[width=0.47\textwidth]{NotreDameAuthPrank}
    \includegraphics[width=0.47\textwidth]{StanAuthPrank}
    \caption{Ovisnost rangiranja prema vrijednosti autoriteta i PageRank-a}
\end{figure}
\newpage
\section{Zaključak}
U ovom radu proučavali smo dva algoritma za rangiranje koji se temelje na 
analizi strukture linkova. Testirali smo brzinu njihove konvergencije te 
zaključili da u većini slučajeva PageRank konvergira značajno brže nego HITS. 
Također, kako jednoj iteraciji PageRank-a treba jedno množenje tipa 
matrica-vektor za razliku od iteracije HITS-a kojoj trebaju dva, iteracija 
PageRank-a je dvostruko brža. 
Konačno, usporedili smo različite mogućnosti rangiranja stranica (na 
temelju vrijednosti hub-a, autoriteta odnosno PageRank-a) te zaključili da 
ne postoji nikakva veza između tih načina rangiranja.

\newpage
\renewcommand{\bibname}{Literatura}
\input{literatura.tex}

\end{document}