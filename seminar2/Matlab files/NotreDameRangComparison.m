file = fopen('../results/web-NotreDame/auth.txt');
auth = textscan(file,'%d %f');
fclose(file);

file = fopen('../results/web-NotreDame/hubs.txt');
hubs = textscan(file,'%d %f');
fclose(file);

file = fopen('../results/web-NotreDame/prank.txt');
prank = textscan(file,'%d %f');

figure;
plot(auth{1}, hubs{1}, 'bx')
xlabel('authority ranking')
ylabel('hubs ranking')
title('NotreDame Auth & Hubs Rank Comparison')

figure;
plot(prank{1}, hubs{1}, 'g.')
xlabel('prank ranking')
ylabel('hubs ranking')
title('NotreDame PRank & Hubs Rank Comparison')

figure;
plot(auth{1}, prank{1}, 'm*')
xlabel('authority ranking')
ylabel('prank ranking')
title('NotreDame Auth & PRank Rank Comparison')

