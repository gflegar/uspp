file = fopen('../results/web-BerkStan/auth.txt');
auth = textscan(file,'%d %f');
fclose(file);

file = fopen('../results/web-BerkStan/hubs.txt');
hubs = textscan(file,'%d %f');
fclose(file);

file = fopen('../results/web-BerkStan/prank.txt');
prank = textscan(file,'%d %f');
fclose(file);

figure;
plot(auth{1}, hubs{1}, 'bx')
xlabel('authority ranking')
ylabel('hubs ranking')
title('BerkStan Auth & Hubs Rank Comparison')

figure;
plot(prank{1}, hubs{1}, 'g.')
xlabel('prank ranking')
ylabel('hubs ranking')
title('BerkStan PRank & Hubs Rank Comparison')

figure;
plot(auth{1}, prank{1}, 'm*')
xlabel('authority ranking')
ylabel('prank ranking')
title('BerkStan Auth & PRank Rank Comparison')

