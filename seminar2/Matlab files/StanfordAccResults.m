file = fopen('../results/web-Stanford/auth-acc.txt');
auth = textscan(file,'%d %f');
fclose(file);

file = fopen('../results/web-Stanford/hubs-acc.txt');
hubs = textscan(file,'%d %f');
fclose(file);

file = fopen('../results/web-Stanford/prank-acc.txt');
prank = textscan(file,'%d %f');
fclose(file);

fig = figure;

semilogy(auth{1}, auth{2}, 'Color', [50 204 10]/255, 'LineWidth', 3, 'LineStyle', '-.')
hold on
semilogy(hubs{1}, hubs{2}, 'Color', [21 24 100]/255, 'LineWidth', 3, 'LineStyle', '--')
hold on
semilogy(prank{1}, prank{2}, 'Color', [201 24 50]/255, 'LineWidth', 2, 'LineStyle', '-')
hold off

xlabel('iteration')
ylabel('accuracy')
legend('authority', 'hub', 'pagerank', 'Location', 'southwest')
title('Stanford convergency results')

response = fig2plotly(fig, 'filename', 'Stanford Graph');
plotly_url = response.url;