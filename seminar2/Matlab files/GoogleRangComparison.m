file = fopen('../results/web-Google/auth.txt');
auth = textscan(file,'%d %f');
fclose(file);

file = fopen('../results/web-Google/hubs.txt');
hubs = textscan(file,'%d %f');
fclose(file);

file = fopen('../results/web-Google/prank.txt');
prank = textscan(file,'%d %f');
fclose(file);

figure;
plot(auth{1}, hubs{1}, 'bx')
xlabel('authority ranking')
ylabel('hubs ranking')
title('Google Auth & Hubs Rank Comparison')

figure;
plot(prank{1}, hubs{1}, 'g.')
xlabel('prank ranking')
ylabel('hubs ranking')
title('Google PRank & Hubs Rank Comparison')

figure;
plot(auth{1}, prank{1}, 'm*')
xlabel('authority ranking')
ylabel('prank ranking')
title('Google Auth & PRank Rank Comparison')
