file = fopen('../results/web-Stanford/auth.txt');
auth = textscan(file,'%d %f');
fclose(file);

file = fopen('../results/web-Stanford/hubs.txt');
hubs = textscan(file,'%d %f');
fclose(file);

file = fopen('../results/web-Stanford/prank.txt');
prank = textscan(file,'%d %f');
fclose(file);

figure;
plot(auth{1}, hubs{1}, 'bx')
xlabel('authority ranking')
ylabel('hubs ranking')
title('Stanford Auth & Hubs Rank Comparison')


figure;
plot(prank{1}, hubs{1}, 'g.')
xlabel('prank ranking')
ylabel('hubs ranking')
title('Stanford PRank & Hubs Rank Comparison')


figure;
plot(auth{1}, prank{1}, 'm*')
xlabel('authority ranking')
ylabel('prank ranking')
title('Stanford Auth & PRank Rank Comparison')


