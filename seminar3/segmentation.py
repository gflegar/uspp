import scipy.optimize as opt
import scipy.sparse as sp
from scipy import *
from scipy.linalg import *


def nmf(A, k, iters):
    m = size(A, 0)
    n = size(A, 1)
    C = abs(randn(m, k))
    G = abs(randn(n, k))
    for i in range(iters):
        for row in range(m):
            C[row,:] = opt.nnls(G, A[row,:])[0]
        for row in range(n):
            G[row,:] = opt.nnls(C, A[:,row])[0]
    return C, G


def simnmf(A, k, alpha, iters):
    n = size(A, 0)
    H = abs(randn(n, k))
    alpha = alpha**0.5;
    for i in range(iters):
        B = concatenate((H, alpha * eye(k,k)))
        D = concatenate((A, alpha * H.T))
        for j in range(n):
            H[j,:] = opt.nnls(B, D[:,j])[0]
        print('error: ', norm(A - H.dot(H.T), 'fro') / norm(A), 'fro')
        print('slope: ', norm((A - H.dot(H.T)).dot(H), 'fro'))
        print('step: ', i)
    return H


def clust(A, k, alpha, iters):
    H = simnmf(A, k, alpha, iters);
    return argmax(H, 1)


def multclust(A, alpha, iters, tol):
    n = size(A, 0)
    C = clust(A, 2, alpha, iters)
    P = argsort(C)
    k = n - count_nonzero(C)
    A = A(P, P)
    if norm(A[:k,:k], 'fro') + norm(A[k:,k:]) > 1 - tol:
        P1, sz1 = multclust(A[:k,:k], alpha, iters, tol)
        P2, sz2 = multclust(A[k:,k:], alpha, iters, tol)
        return (concatenate(P[:k][P1], P[k:][P2]),
                concatenate(sz1, sz2 + k))
    else:
        return P, array([n])


def simmatrix(img, k):
    img = double(img)
    m, n, _ = shape(img)
    #print(m, n)
    els = m*n
    A = sp.lil_matrix((els, els))
    for i, row in enumerate(img):
        for j, elem in enumerate(row):
            #print('(i,j) = ', i, j);
            for si in range(max(i-k, 0), min(i+k+1, m)):
                for sj in range(max(j-k, 0), min(j+k+1, n)):
                    #print('(si, sj) = ', si, sj)
                    col = i + j*m
                    row = si + (sj)*m
                    A[row,col] = exp(-norm(img[i,j,:] - img[si,sj,:]))
    return sp.csc_matrix(A);


