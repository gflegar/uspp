function [] = gentex(G, CG)
    for i = 1:size(G, 1)
        fprintf("%2d", i);
        for j = 1:size(G, 2)
            fprintf("&%.3f&%.3f", G(i,j), CG(i,j));
        end
        fprintf("\\\\\n");
    end
end
