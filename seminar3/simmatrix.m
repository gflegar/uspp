function [A] = simmatrix(img, k)
    m = size(img, 1);
    n = size(img, 2);
    A = spalloc(m*n, m*n, m*n*(2*k+1)^2);
    for i = 1:m
        for j = 1:n
            A = gen_col(A, img, k, i, j);
        end
    end
end

function [A] = gen_col(A, img, k, i, j)
    %fprintf("elem (%d,%d)\n", i, j); fflush(stdout);
    m = size(A,1);
    for oi = -k:k
        if i+oi >= 1 && i+oi <= size(img,1)
            for oj = -k:k
                if j+oj >= 1 && j+oj <= size(img,2)
                    fprintf("ofs: (%d,%d)\n", oi, oj); fflush(stdout);
                    col = i + (j-1)*m;
                    row = col + oi + oj*m;
                    a = double(vec(img(i,j,:)));
                    b = double(vec(img(i+oi,j+oj,:)));
                    A(row,col) = exp(-norm(a-b)^2);
                end
            end
        end
    end
end
