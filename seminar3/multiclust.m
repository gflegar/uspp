function [P, b] = multiclust(A, lambda, tol, maxiter, delta)
    n = size(A,1);
    c = clust(A, 2, lambda, tol, maxiter);
    [~, P] = sort(c);
    k = sum(c == 1);
    A = A(P,P);
    %imshow(A);
    %pause
    if 0 < k && k < n && avgnorm(A(1:k),A(1:k)) > 1-delta && avgnorm(A(k+1:end,k+1:end)) > 1-delta
        %fprintf("(%d/%d)\n", k, n);
        %fprintf("down\n"); fflush(stdout);
        [p1, b1] = multiclust(A(1:k,1:k), lambda, tol, maxiter, delta);
        %fprintf("mid\n"); fflush(stdout);
        [p2, b2] = multiclust(A(k+1:end,k+1:end), lambda, tol, maxiter, delta);
        %fprintf("up\n"); fflush(stdout);
        pp1 = P(1:k);
        pp2 = P(k+1:end);
        P = [pp1(p1); pp2(p2)];
        b = [b1; b2+k];
    else
        b = n;
    end
end

function [x] = avgnorm(A)
    x = mean(sum(A,2));
end
