function [C, G] = nmf(A, k, iters)
    C = abs(randn(size(A,1), k));
    G = abs(randn(size(A,2), k));
    for i = 1:iters
        for row = 1:size(A,1)
            C(row,:) = lsqnonneg(G, A(row,:)', C(row,:)')';
        end
        for row = 1:size(A,2)
            G(row,:) = lsqnonneg(C, A(:,row), G(row,:)')';
        end
        fprintf("Error: %f\n", norm(A - C*G', "fro")); fflush(stdout);
    end
end