function [A, C] = gentest(cs, eps)
    sz  = sum(cs);
    A = eps*abs(randn(sz));
    C = zeros(sz,1);
    start = 1;
    for i = 1:length(cs)
        A(start:start+cs(i)-1,start:start+cs(i)-1) = abs(randn(cs(i)));
        C(start:start+cs(i)-1) = i;
        start = start + cs(i);
    end
    A = A + A';
    P = randperm(sz);
    A = A(P,P);
    C = C(P);
end
