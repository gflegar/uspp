function [err, herr, A, H, c, ps] = evaluate(cs, eps, iters, seed)
    randn('state', seed);
    rand('state', seed);
    [A, c] = gentest(cs, eps);
    %imagesc(A);
    [H, ps, herr] = simnmf(A, length(cs), 1, iters);
    err = zeros(iters,1);
    for i = 1:iters
        eqs = sum(sum(c == c' & ps(:,i) == ps(:,i)'));
        ttl = sum(sum(c == c'));
        err(i) = 1 - eqs/ttl;
    end
end