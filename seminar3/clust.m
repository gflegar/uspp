function [c, H] = clust(A, k, lambda, maxiter)
    H = simnmf(A, k, lambda, maxiter);
    [~, c] = max(H, [], 2);
end
