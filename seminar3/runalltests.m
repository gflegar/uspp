function [E, HE] = runalltests(n, k, iters)
    seeds = [4222473219, 2832825886, 1532822507, 1678312415, 2187757739, ...
             2061118410, 2448798561, 814141002, 2571291403, 710889726];
    s = length(seeds);
    E = zeros(iters, s);
    HE = zeros(iters, s);
    for i = 1:s
        fprintf("Running test %d/%d\n", i, s); fflush(stdout);
        [E(:,i), HE(:,i)] = evaluate(n*ones(1,k), 1e-10, iters, seeds(i));
    end
end
