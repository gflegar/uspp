function [H, part, herr] = simnmf(A, k, lambda, maxiter)
    n = size(A, 1);
    H = abs(randn(n, k));
    part = zeros(n, maxiter);
    herr = zeros(maxiter, 1);
    lambda = sqrt(lambda);
    for i = 1:maxiter
        B = [H; lambda * eye(k,k)];
        D = [A; lambda * H'];
        for j = 1:n
            H(j,:) = lsqnonneg(B, D(:,j), H(j,:)')';
        end
        [~, tmp] = max(H,[], 2);
        part(:,i) = tmp;
        herr(i) = norm(A - H*H', 'fro') / norm(A, 'fro');
        %slope = (A-H*H')*H;
        %t = H < eps;
        %slope(t) = max(0, slope(t));
        %fprintf("Error: %f\n", norm(A - H*H', "fro") / norm(A, "fro"));
        %fprintf("Slope: %f\n", norm(slope, 'fro'));
        %fflush(stdout);
        %if norm(slope, 'fro') < tol
        %    break
        %end
    end
end
