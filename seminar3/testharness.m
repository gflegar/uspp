function [R, CR] = testharness()
    ks = [2, 3, 4, 5];
    ns = [10, 20, 30, 40, 50];
    iters = 30;
    R = zeros(iters, length(ns), length(ks));
    CR = zeros(iters, length(ns), length(ks));
    for i = 1:length(ks)
        for j = 1:length(ns)
            k = ks(i);
            n = ns(j);
            fprintf("k = %d; n = %d\n", k, n); fflush(stdout);
            [E, HE] = runalltests(n, k, iters);
            %disp(E);
            R(:,j,i) = mean(E, 2);
            CR(:,j,i) = mean(HE, 2);
            disp(R(:,j,i));
        end
    end
end