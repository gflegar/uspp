if Pkg.installed("PyPlot") == nothing
    Pkg.add("PyPlot")
end

using PyPlot

function usage()
    prog = basename(Base.source_path())
    println("""
    Usage: $prog dim infile
        Display a matrix of dimension `dim` x `dim` saved in file `infile`
        as a grayscale image.
    """)
    exit(1)
end

function showmatrix(dim::Int, filename::AbstractString)
    S = open(fp -> read(fp, Float64, (dim, dim)), filename)
    S = S ./ maximum(S, 2)
    PyPlot.imshow(S, :gray_r, interpolation="none")
end

