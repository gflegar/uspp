#!/usr/bin/julia

module BlockStohastic
if Pkg.installed("StatsBase") == nothing
    Pkg.add("StatsBase");
end

using StatsBase

"""
Generate a random row-stohastic matrix.

### Args:
* `dim` -- Matrix size.

### Returns:
A row-stohastic matrix of size `dim` x `dim`.
"""
function stohastic(dim::Int)
    A = rand(dim, dim)
    A ./ reducedim(+, A, 2)
end

"""
Generate a random block-stohastic matrix.

### Args:
* `dim` --  Matrix size.
* `blocks` --  Number of blocks.

### Returns:
A pair `(S, b)` where S is a block-stohastic matrix of size `dim` x `dim`
with blocks starting at diagonal positions in `b`.
"""
function block_stohastic(dim::Int, blocks::Int)
    S = zeros(dim, dim)
    limits = [sample(2:dim, blocks-1, replace=false, ordered=true); dim + 1]
    last = 1
    for i in limits
        S[last:i-1,last:i-1] = stohastic(i - last)
        last = i
    end
    S, [1; limits[1:end-1]]
end

"""
Perturbe a given stohastic matrix, but leave it stohastic.

### Args:
* `S` -- A stohastic matrix.
* `eps` -- perturbation size.

### Returns:
A slightly perturbed stohastic matrix.
"""
function perturbe{T<:Real}(S::Matrix{T}, eps::T)
    (1-eps)*S + eps*stohastic(size(S, 1))
end

"""
Randomly permute rows and columns of a given matrix.

### Args:
* `A` -- A matrix with equal dimensions.

### Returns:
A pair `(Ap, p)` where `Ap` is a obtained from `A` by permuting its rows and
columns with permutation `p`.
"""
function permute{T<:Real}(A::Matrix{T})
    if size(A,1) != size(A,2)
        error("A is not square.")
    end
    p = randperm(size(A, 1))
    A[p,p], p
end

"""
Print usage message.
"""
function usage()
    prog = basename(Base.source_path())
    println("""
    Usage: $prog dim blocks eps matrixfile blocksfile permfie
        Generate a random block-stohastic double precision matrix,
        perturbe it, apply a permutation of rows and columns and save it to
        `matrixfile`.
        Block data is saved to `blocksfile` and appplied permutation of rows
        and columns to `permfile`.

        dim        - matrix size
        blocks     - number of blocks
        eps        - size of perturbation

    Example:
        $prog 10 3 1e-6 S.dat blocks.dat perm.dat
          => creates a 10x10 matrix with 3 blocks and a perturbation of order
             of magnitude 1e-6 and saves it to S.dat. Block and permutation
             information is saved to blocks.dat and perm.dat
    """)
    exit(1)
end

"""
Generate a permuted and slightly perturbed block-stohastic matrix and write it
to a file. Aditionaly, write the starting indeces of blocks and the permutation
matrix to files.

### Args:
* dim -- size of the matrix
* blocks -- number of diagonal blocks
* eps  -- perturbation size
* matrixfile -- matrix output file name
* blocksfile -- block data output file name
* permfile -- permutation vector output file name
"""
function generate(dim::Int, blocks::Int, eps::Real, matrixfile::AbstractString,
                  blocksfile::AbstractString, permfile::AbstractString)

    S, b = block_stohastic(dim, blocks)
    S, p = permute(perturbe(S, eps))

    open(matrixfile, "w") do fp
        write(fp, S)
    end
    open(blocksfile, "w") do fp
        write(fp, [length(b); b])
    end
    open(permfile, "w") do fp
        write(fp, p)
    end

    display(S)
    println()
    S
end

end

if !isinteractive()
    length(ARGS) == 6 || BlockStohastic.usage()
    BlockStohastic.generate(parse(Int, ARGS[1]), parse(Int, ARGS[2]),
                            parse(Float64, ARGS[3]), ARGS[4], ARGS[5], ARGS[6])
end
