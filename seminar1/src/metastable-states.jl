#!/usr/bin/julia

module MetastableStates

"""
```
norm(S, startidx, endidx)
```
Calculate **1**-norm of a principal submatrix of the given matrix.

### Args:
* `S` -- a matrix
* `startidx` -- submatrix's starting index
* `endidx` -- submatrix's ending index

### Returns:
Norm of the matrix `S[startidx:endidx,startidx:endidx]`.
"""
function norm{T<:Real}(S::Matrix{T}, startidx::Int, endidx::Int)
    sum(S[startidx:endidx,startidx:endidx]) / (endidx - startidx + 1)
end

"""
```
splitblocks(S, delta, startidx, endidx)
```
Try to split a submatrix of the given stohastic matrix into two uncoupled
blocks based on the second largest left singular vector of the submatrix.

### Args:
* `S` -- a stohastic matrix
* `delta` -- tolerance value, used to determine if a coefficient in the
    singular vector is zero
* `startidx` -- starting index of the submatrix
* `endidx` -- ending index of the submatrix

### Returns:
A pair `(perm, split)` where `perm` is a permutation that should be applied to
matrix `S` to reveal a hidden block-structure and `split` is the index where S
should be split into two blocks.
"""
function splitblocks{T<:Real}(S::Matrix{T}, delta::T, startidx::Int,
                              endidx::Int)
    Ss = S[startidx:endidx,startidx:endidx]
    u2 = svdfact(Ss)[:U][:,2]
    perm = sortperm(u2)
    u2 = u2[perm]
    perm = [1:startidx-1; perm + startidx - 1; endidx+1:size(S,1)]

    split = findfirst(x -> x > -delta, u2) + startidx - 1
    if split == startidx
        split = findfirst(x -> x > delta, u2) + startidx - 1
    end

    perm, split
end

"""
```
splitblocks2(S, delta, startidx, endidx)
```
Try to split a submatrix of the given stohastic matrix into two uncoupled
blocks based on the second smalest left singular vector of the submatrix of
S - I.

### Args:
* `S` -- a stohastic matrix
* `delta` -- tolerance value, used to determine if a coefficient in the
    singular vector is zero
* `startidx` -- starting index of the submatrix
* `endidx` -- ending index of the submatrix

### Returns:
A pair `(perm, split)` where `perm` is a permutation that should be applied to
matrix `S` to reveal a hidden block-structure and `split` is the index where S
should be split into two blocks.
"""
function splitblocks2{T<:Real}(S::Matrix{T}, delta::T, startidx::Int,
                               endidx::Int)
    Ss = S[startidx:endidx,startidx:endidx]
    v = svdfact(Ss - eye(Ss))[:V][:,end-1]
    perm = sortperm(v)
    v = v[perm]
    perm = [1:startidx-1; perm + startidx - 1; endidx+1:size(S,1)]

    split = findfirst(x -> x > -delta, v) + startidx - 1
    if split == startidx
        split = findfirst(x -> x > delta, v) + startidx - 1
    end

    perm, split
end

"""
```
isuncoupled(S, delta, startidx, endidx, split)
```
Determine if two subblocks of a stohastic matrix are uncoupled.

### Args:
* `S` -- a stohastic matrix
* `delta` -- tollerance value for uncoupling detection
* `startidx` -- starting index of the first subblock
* `endidx` -- endingindex of the second subblock
* `split` -- starting index of the second subblock, and first index after the
    end of the first subblock

### Returns:
`true` if subblocks are uncoupled, `false` otherwise.
"""
function isuncoupled{T<:Real}(S::Matrix{T}, delta::T, startidx::Int,
                              endidx::Int, split::Int)
    (startidx < split <= endidx && norm(S, startidx, split-1) > 1-delta &&
        norm(S, split, endidx) > 1-delta)
end

"""
```
identify(S, delta, tol)
```

Identify meta-stable blocks in the given matrix.

### Args:
* `S` -- a stohastic matrix
* `delta` -- tolerance level used to identify uncoupled blocks
* `tol` -- tolerance level used to identify 0-valued coefficients in singular
           vectors
* `method` -- method used for spliting blocks: 1 for SVD method, 2 for spectral
              method
### Returns:
A tuple `(Sp, perm, blocks)` where `Sp = perm * S * perm^T` is a
block-stohastic matrix with blocks starting at indeces returned in vector
`blocks`. `perm` is returnd as a vector of column-indeces of ones in a
permutation matrix.
"""
function identify{T<:Real}(S::Matrix{T}, delta::T, tol::T, method::Int)
    dim = size(S, 1)
    stack = [(1, dim)]
    perm = collect(1:dim)
    blocks = []
    while length(stack) > 0
        startidx, endidx = pop!(stack)
        if endidx == startidx
            push!(blocks, startidx)
            continue
        end

        if method == 1
            fperm, split = splitblocks(S, tol, startidx, endidx)
        else
            fperm, split = splitblocks2(S, tol, startidx, endidx)
        end

        S = S[fperm,fperm]
        perm = perm[fperm]

        if isuncoupled(S, delta, startidx, endidx, split)
            push!(stack, (split, endidx))
            push!(stack, (startidx, split-1))
        else
            push!(blocks, startidx)
        end
    end
    S, perm, blocks
end

function usage()
    prog = basename(Base.source_path())
    println("""
    Usage: $prog dim delta tol infile outfile permfile blocksfile method
        Reveal the hidden block structure in a permuted and perturbed
        block-stohastic matrix read from `infile` and write the permuted matrix
        to `outfile`. Applied permutation is saved to `permfile` and starting
        indeces of blocks to `blocksfile`. Number of blocks is saved in the
        first 8 bytes of `blocksfile`. Method is a method number used to 
    """)
    exit(1)
end

"""
```
main(dim, delta, tol, infile, outfile, permfile, blocksfile, method)
```

A wraper function for identify() for easier invocation from command line.
Call usage() function for details.
"""
function main(dim::Int, delta::Float64, tol::Float64, infile::AbstractString,
              outfile::AbstractString, permfile::AbstractString,
              blocksfile::AbstractString, method::Int = 1)
    S = open(fp -> read(fp, Float64, (dim, dim)), infile)
    tic()
    Sp, perm, blocks = identify(S, delta, tol, method)
    elapsed = toq()
    open(outfile, "w") do fp
        write(fp, Sp)
    end
    open(permfile, "w") do fp
        write(fp, perm)
    end
    open(blocksfile, "w") do fp
        write(fp, [length(blocks); blocks])
    end
    Sp, elapsed
end

function avgnorm(dim::Int, matrixfile::AbstractString,
                 blocksfile::AbstractString)
    S = open(fp -> read(fp, Float64, (dim, dim)), matrixfile);
    blocks = []
    k = 0
    open(blocksfile) do fp
        k = read(fp, Int)
        blocks = read(fp, Int, k)
    end
    normsum = 0
    for i = 1:k-1
        normsum += norm(S, blocks[i], blocks[i+1] - 1)
    end
    normsum += norm(S, blocks[k], dim)
    normsum / k
end

function numblocks(blocksfile::AbstractString)
    open(fp -> read(fp, Int), blocksfile)
end

end

if !isinteractive()
    length(ARGS) == 8 || MetastableStates.usage()
    S = MetastableStates.main(parse(Int, ARGS[1]), parse(Float64, ARGS[2]),
                              parse(Float64, ARGS[3]), ARGS[4], ARGS[5],
                              ARGS[6], ARGS[7], parse(Int, ARGS[8]))
    display(S)
    println()
end

