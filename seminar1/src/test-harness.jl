#!/usr/bin/julia


module TestHarness

include("metastable-states.jl")
include("rnd-blk-stohastic.jl")

type TestRecord
    matrixfile::ASCIIString
    blocksfile::ASCIIString
    permfile::ASCIIString
    dim::Int
    blocks::Int
    pert::Float64
end

function Base.write(io::IO, record::TestRecord)
    write(io, record.matrixfile)
    write(io, record.blocksfile)
    write(io, record.permfile)
    write(io, record.dim)
    write(io, record.blocks)
    write(io, record.pert)
end

function Base.read(io::IO, tp::Type{TestRecord})
    TestRecord(
        read(io, ASCIIString),
        read(io, ASCIIString),
        read(io, ASCIIString),
        read(io, Int),
        read(io, Int),
        read(io, Float64)
    )
end

function create_tests(dimstart::Int, dimend::Int, diminc::Int,
                      pertstart::Float64, pertend::Float64, pertinc::Float64,
                      minblocks::Int, maxblocks::Int)
    test_data = TestRecord[]
    if !isdir("tests")
        mkdir("tests")
    end
    for dim = dimstart:diminc:dimend
        pert = pertstart
        pertind = 1
        while (pert <= pertend)
            suffix = "_$(dim)_$(pertind).dat"
            blks = rand(minblocks:min(maxblocks, dim))
            record = TestRecord("A$suffix", "blocks$suffix",
                                "perm$suffix", dim, blks, pert)
            println("Creating file $(record.matrixfile)")
            BlockStohastic.generate(record.dim, record.blocks, record.pert,
                                    "tests/$(record.matrixfile)",
                                    "tests/$(record.blocksfile)",
                                    "tests/$(record.permfile)")
            push!(test_data, record)
            pertind += 1
            pert *= pertinc
        end
    end
    #open("test_data.dat", "w") do fp
    #    write(fp, size(test_data, 1))
    #    write(fp, test_data)
    #end
    test_data
end


function create_test_data(dimstart::Int, dimend::Int, diminc::Int,
                      pertstart::Float64, pertend::Float64, pertinc::Float64)
    test_data = TestRecord[]
    for dim = dimstart:diminc:dimend
        pert = pertstart
        pertind = 1
        while (pert <= pertend)
            suffix = "_$(dim)_$(pertind).dat"
            blks = MetastableStates.numblocks("tests/blocks$suffix")
            record = TestRecord("A$suffix", "blocks$suffix",
                                "perm$suffix", dim, blks, pert)
            push!(test_data, record)
            pertind += 1
            pert *= pertinc
        end
    end
    #open("test_data.dat", "w") do fp
    #    write(fp, size(test_data, 1))
    #    write(fp, test_data)
    #end
    test_data
end
function read_test_data()
    td = []
    open("test_data.dat", "w") do fp
        size = read(fp, Int)
        td = read(fp, TestRecord, size)
    end
    td
end

function run_tests(test_data, algtype::Int,
                   resdir::AbstractString)
    if !isdir(resdir)
        mkdir(resdir)
    end

    results = []

    for data in test_data
        println("Runing test $(data.matrixfile)")
        _, time = MetastableStates.main(
                data.dim, data.pert, 1e-12,
                "tests/$(data.matrixfile)",
                "$resdir/$(data.matrixfile)",
                "$resdir/$(data.permfile)",
                "$resdir/$(data.blocksfile)",
                algtype)
        acc = MetastableStates.avgnorm(
                data.dim,
                "$resdir/$(data.matrixfile)",
                "$resdir/$(data.blocksfile)"
        )
        orig_blks = MetastableStates.numblocks("tests/$(data.blocksfile)")
        calc_blks = MetastableStates.numblocks("$resdir/$(data.blocksfile)")

        push!(results, (time, acc, orig_blks, calc_blks, data.dim, data.pert))
        println("Time             : $time \n",
                "Accuracy         : $acc \n",
                "Original blocks  : $orig_blks \n",
                "Calculated blocks: $calc_blks")
    end

    results
end

function get_found_blocks(results)
    blks = []
    for touple in results
        push!(blks, touple[4])
    end
    blks
end

function get_orig_blocks(results)
    blks = []
    for touple in results
        push!(blks, touple[3])
    end
    blks
end

function get_acur(results)
    acur = []
    for touple in results
        push!(acur, touple[2])
    end
    acur
end

function get_time(results)
    time = []
    for touple in results
        push!(time, touple[1])
    end
    time
end

end
