Identification of meta-stable states of Markov chains
=====================================================

Algorithms
----------
In this seminar we studied algorithms for identification of meta-stable states 
of Markov chains. We implemented and compared two algorithms:

* The first one is an SVD approach proposed in [1].
* The second one is a combination of the recursive strategy from [1] and the
  spectral method from [2].

Implementation
--------------
We implemented our algorithms using the _Julia_ programming language.
Implementation of both algorithms can be found
[here](https://bitbucket.org/gflegar/uspp/src/master/seminar1/src/metastable-states.jl).

Tests
-----
We tested the speed and accuracy of our algorithms on 60 randomly generated
test cases of various dimensions and perturbation sizes.
Test data and test results can be found in `src/tests`, `src/res1` and
`src/res2` directories (in binary format).
Details about test generation are provided in [3].

Results
-------
We found that the second algorithm is both more accurate and faster than the
first one. More details can be found in [3].
As an example we show images of the original `A_600_9.dat` matrix and the
permutations found by the two algorithms.

### Original matrix:
![Original A_600_9](https://bitbucket.org/gflegar/uspp/raw/master/seminar1/A_600_9_orig.png)

### SVD algorithm result:
![Permuted A_600_9 with SVD approach](https://bitbucket.org/gflegar/uspp/raw/master/seminar1/A_600_9_SVD.png)

### Spectral algorithm result:
![Permuted A_600_9 with spectral approach](https://bitbucket.org/gflegar/uspp/raw/master/seminar1/A_600_9_spec.png)

References
----------

[1] D. Fritzsche, V. Mehrmann, B. Szyld, E. Virnik, _An SVD approach to
identifying metastable states of Markov chains_, ETNA. Electronic
Transactions on Numerical Analysis [electronic only], 29 (2008), pp. 46–69,
http://www.emis.ams.org/journals/ETNA/vol.29.2007-2008/pp46-69.dir/pp46-69.pdf

[2] Jacobi, Martin Nilsson. _A robust spectral method for finding lumpings and
meta stable states of non-reversible Markov chains_, ETNA. Electronic
Transactions on Numerical Analysis [electronic only], 37 (2010), pp. 296-306,
http://www.emis.ams.org/journals/ETNA/vol.37.2010/pp296-306.dir/pp296-306.pdf

[3] Our seminar (in croatian), https://bitbucket.org/gflegar/uspp/raw/master/seminar1/seminar/seminar.pdf
