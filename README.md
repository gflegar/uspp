Introduction to data mining ("Uvod u složeno pretraživanje podataka")
=====================================================================

Seminars for the course held at University of Zagreb, Department of
Mathematics.

Repository structure:
---------------------
* [`seminar1`](https://bitbucket.org/gflegar/uspp/src/master/seminar1/)
    -- Identification of meta-stable states of Markov chains
* [`seminar2`](https://bitbucket.org/gflegar/uspp/src/master/seminar2/)
    -- Link Analysis: Hubs and authorities on the World Wide Web

